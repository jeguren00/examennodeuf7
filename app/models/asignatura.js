const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Docente = require('./docente.js');
const Alumno = require('./alumnos.js');

var asignatura = new Schema({
    nombre: String,
    numHoras: String,
    docente: Docente,
    alumnos: [Alumno]
});

const Asignatura = mongoose.model('Asignatura', asignatura);
module.exports = Asignatura;
