const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var alumno = new Schema({
    nombre: String,
    apellido: String,
});

const Alumno = mongoose.model('Alumno', alumno);
module.exports = Alumno;
