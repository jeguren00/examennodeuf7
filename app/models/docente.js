const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var docente = new Schema({
    nombre: String,
    apellido: String,
});

const Docente = mongoose.model('Docente', docente);
module.exports = Docente;