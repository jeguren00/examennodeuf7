var express = require("express");
var path = require("path");
var router = express.Router();

var asignatura = require('../controllers/asignaturas.js');
var alumno = require('../controllers/alumnos.js');
var docente = require('../controllers/docentes.js');

router.get("/", async (req, res, next) => {
    res.render("index.pug");
});

router.get('/asignatura/new',alumno.get);
router.get('/asignatura/new',docente.get);
router.get('/asignatura/new',function(req,res){
    res.render("newAsignatura.pug", {alumnos:req.docentes, docentes:req.alumnos});
});

router.post('/asignatura/save',asignatura.save);
//NOT IMPLEMENTED
//router.post('/asignatura/update',asignatura.update);
//router.post('/asignatura/delete',asignatura.delete);

module.exports = router;