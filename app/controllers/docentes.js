const Docente = require("../models/docente");

exports.get = async (req, res, next) => { 
    var docentes = await Docente.find();
    req.docentes = docentes;
    next();
}