const Alumno = require("../models/alumnos")

exports.get = async (req, res, next)  =>  {
    var alumnos = await Alumno.find();
    req.alumnos = alumnos;
    next();
}