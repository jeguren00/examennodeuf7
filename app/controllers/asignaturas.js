const Docente = require("../models/docente");
const Alumno = require("../models/alumnos");
const Asignatura = require("../models/asignatura");

exports.save = async (req, res, next) => {
    let docente = await Docente.findById(req.body.teacher);
    //this command theoreticaly outputs the list of alumnos id selected checkboces, and what i wanted to do is create an array of alumnos object and save it using this var
    let listaAlum = req.body.alumno;

    let asignatura = new User({nombre: req.body.aName, numHoras: req.body.numHoras, docente: docente, alumnos:listaAlum});
    asignatura.save();
    res.redirect("/")
}

exports.list = async (req, res, next) => {
    var assignaturas = await Asignatura.find();
    req.assignaturas = assignaturas;
    next();
}

//where ids is an array of asignatura id's
exports.delete = async (req, res, next) => {
    await Asignatura.deleteMany(req.ids);
    next();
}


